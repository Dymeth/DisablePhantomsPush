package ru.dymeth.disablephantomspush;

import org.bukkit.Bukkit;
import org.bukkit.entity.Phantom;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class DisablePhantomsPush extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
    }

    private boolean cancelVelocityEvent = false;

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    private void on(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (!(event.getDamager() instanceof Phantom)) return;
        this.cancelVelocityEvent = true;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void on(PlayerVelocityEvent event) {
        if (!this.cancelVelocityEvent) return;
        this.cancelVelocityEvent = false;
        event.setCancelled(true);
    }
}
